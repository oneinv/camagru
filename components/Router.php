<?php

class Router
{
	private $routes;

	public function __construct()
	{
		$routesPath= ROOT.'/config/routes.php';
		$this->routes = include($routesPath);

	}
	private function getURI()
	{
		if (!empty($_SERVER['REQUEST_URI'])) {
			$uri = trim($_SERVER['REQUEST_URI'], '/');
		}
		return $uri;
	}
	public function find()
	{
		$uri = $this->getURI();
		foreach ($this->routes as $uriPattern => $path) { // проверяю есть ли такой запрос в routes.php
			if(preg_match("~$uriPattern~", $uri)) {
				// создаю внутренний путь из внешнего
				$internalRoute = preg_replace("~$uriPattern~", $path, $uri);
				// определяю какой контроллер и экшен будут обрабатывать запрос
				$segment = explode('/', $internalRoute);
				$controllerName = array_shift($segment).'Controller';
				$actionName = 'action'.ucfirst(array_shift($segment));
				$parametres = $segment;

				// подключаю файл нужного контроллера
				$controllerFile = ROOT.'/controller/'.$controllerName.'.php';
				$ret =  [
					'parametres' => $parametres,
					'controllerName' => $controllerName,
					'controllerFile' => $controllerFile,
					'actionName' => $actionName,
				];
					return $ret;
			}
		}
		return false;
	}
	public function run()
	{
		$array = $this->find();
			if (file_exists($array['controllerFile'])) {
					include_once($array['controllerFile']);
				//создаю объект, вызываю экшн
				$controllerObject = new $array['controllerName'];
				// использую этo для адекватной передачи параметров в функцию
				$result = call_user_func_array(array($controllerObject, $array['actionName']), $array['parametres']);
			} else
				header("Location: /404.php");

	}
}

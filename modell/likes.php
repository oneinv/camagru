<?php

class Likes
{
		public static function getAllLikes($id)
		{
			$db = Db::getConnection();

			$sql = "SELECT COUNT(*)  FROM likes WHERE id = :id";

			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			$likes = $result->fetch();
			return ($likes[0]);
		}

		public static function addLike($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "INSERT INTO likes(id, user_id, liked)
		 			VALUES (:id, :user_id, :liked)";
			$result = $db->prepare($sql);
			$hmm = 1;
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':liked', $hmm, PDO::PARAM_INT);
			return ($result->execute());
		}

		public static function isLikedByUser($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "SELECT COUNT(*) FROM likes WHERE id=:id AND user_id=:user_id";

			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->execute();
			$liked = $result->fetch();
			return ($liked[0]);
		}

		public static function deleteLike($id, $user_id)
		{
			$db = Db::getConnection();
			$sql = "DELETE FROM likes WHERE id=:id AND user_id=:user_id";
			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			return ($result->execute());
		}

		public static function getAllInfo()
		{
			$db = Db::getConnection();
			$sql = "SELECT count(*) FROM likes";
			$result =$db->prepare($sql);
			$result->execute();
	 		$row = $result->fetch();
			return $row['0'];
		}

}

<?php

class photo
{
	public static function addPhoto($userId, $login, $image)
	{
		$db = Db::getConnection();
		$sql = "INSERT INTO photo(user_id, login, image)
				VALUES (:user_id, :login, :image)";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':image', $image, PDO::PARAM_STR);
		$result->bindParam(':user_id', $userId, PDO::PARAM_INT);
		return ($result->execute());
	}

	public static function updatePhoto($id)
	{
		$db = Db::getConnection();
		$sql = "UPDATE photo SET image = :name WHERE id= :id";

		$name = $id . ".jpg";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
		return ($result->execute());
	}

	public static function addAvatar($userName, $name)
	{
		$db = Db::getConnection();
		$sql = "UPDATE user SET avatar = :name WHERE login=:userName";
		$result = $db->prepare($sql);
		$result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':userName', $userName, PDO::PARAM_INT);
		return ($result->execute());
	}
	public static function getLastId()
	{
		$db = Db::getConnection();
		$sql = "SELECT id from photo";

		$result = $db->prepare($sql);
		$ids = array();
		$i = 0;
		$result->execute();
		while ($row = $result->fetch()) {
            $ids[$i]['id'] = $row['id'];
			$i++;
		}
		if($i == 0)
			return 0;
		else
			return $ids[$i - 1]['id'];
	}

	public static function getLoginByPicture($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT login FROM photo WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$ret = $result->fetch();
		return $ret['login'];
	}

	public static function deletePhoto($id, $user_id)
	{
		$db = Db::getConnection();
		$sqlL = "DELETE FROM likes WHERE id=:id";
		$sqlC = "DELETE FROM comms WHERE id=:id";
		$delcom = $db->prepare($sqlC);
		$delcom->bindParam(':id', $id, PDO::PARAM_INT);
		$delcom->execute();
		$dellike = $db->prepare($sqlL);
		$dellike->bindParam(':id', $id, PDO::PARAM_INT);
		$dellike->execute();
		$sql = "DELETE FROM photo WHERE id=:id AND user_id=:user_id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		return ($result->execute());
	}

}

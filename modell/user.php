<?php

require_once(ROOT.'/PHPMailer/PHPMailer.php');
require_once(ROOT.'/PHPMailer/Exception.php');
require_once(ROOT.'/PHPMailer/SMTP.php');
use PHPMailer\PHPMailer\PHPMailer;

class User
{


	public static function register($login, $email, $password) {

        $db = Db::getConnection();
		$role = "user";
		$token = bin2hex(random_bytes(10));
		$avatar = "default.jpg";
        $sql = 'INSERT INTO user (login, email, password, avatar, role, token, is_auth) '
                . 'VALUES (:login, :email, :password, :avatar, :role, :token, 0)';
		$path = ROOT . '/upload/users/';
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
		$result->bindParam(':avatar', $avatar, PDO::PARAM_STR);
		$result->bindParam(':role', $role, PDO::PARAM_STR);
		$result->bindParam(':token', $token, PDO::PARAM_STR);
		if (!file_exists($path.$login)) {
			mkdir($path.$login);
			mkdir($path.$login."/avatar");
		}
        return $result->execute();
    }


	public static function checkName($name)
	{
		if (strlen($name) >= 3 && ctype_alnum($name))
			return true;
		else
			return false;
	}

	public static function checkPswd($password)
	{
		if (strlen($password) >= 6 && ctype_alnum($password))
			return true;
		else
			return false;
	}

	public static function checkMail($email)
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL))
			return true;
		else {
			return false;
		}
	}

	public static function checkPhone($phone)
    {
        if (strlen($phone) >= 10 && strlen($phone) <= 14) {
            return true;
        }
        return false;
    }

	public static function checkConfirm($login, $password)
	{
		$db = Db::getConnection();

        $sql = 'SELECT is_auth FROM user WHERE login =:login AND password = :password';

        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
		$result->execute();
		$ret = $result->fetch();
		return $ret['is_auth'];
	}

	public static function checkMailExists($email)
	{
		$db = Db::getConnection();

		$sql = 'SELECT COUNT(*) from user WHERE email = :email';
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->execute();

		if($result->fetchColumn())
			return true;
		return false;
	}

	public static function checkLoginExists($login)
	{
		$db = Db::getConnection();

		$sql = 'SELECT COUNT(*) from user WHERE login = :login';
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();
		if($result->fetchColumn())
			return true;
		return false;
	}

	public static function checkUserData($login, $password)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM user WHERE login =:login AND password = :password';
        $result = $db->prepare($sql);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        $result->execute();
        $user = $result->fetch();
        if ($user)
            return $user['login'];
        return false;
    }

	public static function checkLog()
	{
		if (isset($_SESSION['user'])) {
			return $_SESSION['user'];
		}
		header("Location: /user/login");
	}

	public static function isGuest()
	{
		if (isset($_SESSION['user']))
			return false;
		return true;
	}

	public static function auth($login)
	{
		$_SESSION['user'] = $login;
	}

	public static function getUserByID($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT login from user WHERE id=:id";
		$result = $db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();

		$lol = $result->fetch();
		return $lol['login'];
	}

	public static function getIdByLogin($login)
	{
		$db = Db::getConnection();
		$sql = "SELECT id from user WHERE login=:login";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();

		$login= $result->fetch();
		return $login['id'];
	}

	public static function getAvaByLogin($login)
	{
		$db = Db::getConnection();
		$sql = "SELECT avatar from user WHERE login=:login";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();
		$default = "/upload/default.jpg";
		$avatar = "/upload/users/". $login.'/'.'avatar/'."avatar".".jpg";
		$login= $result->fetch();
		if ($login['avatar'] == "avatar.jpg")
			return ($avatar);
		else
			return $default;
	}

	public static function recoverPswd($email, $password)
	{
		$db = Db::getConnection();
        $sql = "UPDATE user SET password = :password WHERE email = :email";

		$result = $db->prepare($sql);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		return $result->execute();
	}

	public static function getUserFullInfo($login)
	{
		$db = Db::getConnection();
		$sql = "SELECT email, password, token, notific FROM user WHERE login=:login";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->execute();
		$row = $result->fetch();
		$userData['email'] = $row['email'];
		$userData['password'] = $row['password'];
		$userData['token'] = $row['token'];
		$userData['notific'] = $row['notific'];
		return $userData;
	}

	public static function editData($id, $login, $password, $email, $notific)
	{
        $db = Db::getConnection();
        $sql = "UPDATE user
            SET login= :login, password = :password, email = :email, notific =:notific
            WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->bindParam(':notific', $notific, PDO::PARAM_INT);
        $result->bindParam(':login', $login, PDO::PARAM_STR);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
		User::auth($login);
        return $result->execute();

	}

	public static function getToken($email)
	{
		$db = Db::getConnection();
		$sql = "SELECT token FROM user WHERE email = :email";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->execute();
		$ret = $result->fetch();
		return $ret['token'];
	}

	public static function isNotific($login)
	{
		$db = Db::getConnection();
		$sql = "SELECT notific FROM user WHERE login = :login";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_INT);
		$result->execute();
		$ret = $result->fetch();
		return $ret['notific'];
	}

	public static function getMail($email)
	{
		$db = Db::getConnection();
		$sql = "SELECT email FROM user WHERE email = :email";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		$result->execute();

		$email = $result->fetch();
		return $email['email'];
	}

	public static function getMailByLogin($login)
	{
		$db = Db::getConnection();
		$sql = "SELECT email FROM user WHERE login=:login";
		$result = $db->prepare($sql);
		$result->bindParam(':login', $login, PDO::PARAM_INT);
		$result->execute();

		$email = $result->fetch();
		return $email['email'];
	}

	public static function activateUser($email)
	{
		$db = Db::getConnection();
		$sql = "UPDATE user SET is_auth = 1 WHERE email = :email";
		$result = $db->prepare($sql);
		$result->bindParam(':email', $email, PDO::PARAM_STR);
		return $result->execute();
	}

	public static function sendMail($email, $token, $doing)
	{
		$HTTP_HOST = $_SERVER['HTTP_HOST'];
		$user_mail = $email;
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->Host="smtp.gmail.com";
		$mail->SMTPAuth= true;
		$mail->Username ="testingcamagru@gmail.com";
		$mail->Password = "ZZ002191";
		$mail->From="testingcamagru@gmail.com";
		$mail->FromName= "no-reply@camagru.com";
		$mail->addAddress($user_mail, "HELLO");
		$mail->isHTML(true);
		if ($doing == "activate") {
			$mail->Subject = "Please verify your account email";
			$mail->Body = "
		Please click on the link below to confirm your registration: <br><br>
		<a href='http://$HTTP_HOST/user/confirm/?email=$user_mail&token=$token'>Click Here</a>";
		}
		else if ($doing =="recover") {
			$mail->Subject = "Recover password";
			$mail->Body = "
			Please click on the link below to Recover your password on camagru: <br><br>
			<a href='http://$HTTP_HOST/user/recover/?email=$user_mail&token=$token'>Click Here</a>";
		}
		else if ($doing = "notific")
		{
			$mail->Subject = "Comment under your photo";
			$mail->Body = "Hello, Dear. One of your photo were commented. Come on Camagru and look:<br><br>
			<a href='http://$HTTP_HOST/photo'>Click Here</a>";
		}
		$mail->send();
	}
	public static function getAllInfo()
	{
		$db = Db::getConnection();
		$sql = "SELECT count(*) FROM user";
		$result =$db->prepare($sql);
		$result->execute();
 		$row = $result->fetch();
		return $row['0'];
	}
}

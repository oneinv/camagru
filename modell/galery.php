<?php

class Galery
{
	const SHOW_BY_DEFAULT = 6;

	public static function getNumPictures()
	{
		$db = Db::getConnection();
		$sql = "SELECT count(id) FROM photo" ;

		$result =$db->prepare($sql);
		$count = 0;
		$result->execute();
		$count = $result->fetch();
		return $count[0];
	}

	public static function getAllPictures($page)
	{
		$page = intval($page);
		$offset = ($page - 1) * self::SHOW_BY_DEFAULT;
		$db = Db::getConnection();
		$sql = "SELECT * FROM photo ORDER BY id DESC LIMIT ".self::SHOW_BY_DEFAULT. " OFFSET ".$offset ;
		$images = array();
		$i = 0;
		$result =$db->prepare($sql);
		$result->execute();
		while ($row = $result->fetch()) {
            $images[$i]['login'] = $row['login'];
			$images[$i]['image'] = $row['image'];
			$images[$i]['id'] = $row['id'];
            $i++;
        }
		return $images;
	}

	public static function getImage($id, $login)
    {
        $noImage = 'no-image.jpg';
        $path = '/upload/users/';
		$log = $login. "/";
        $pathToProductImage = $path . $log. $id. '.jpg';
        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToProductImage)) {
            return $pathToProductImage;
        }
        return $path . $noImage;
    }

	public static function getImageById($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT image, login FROM photo WHERE id=:id";

		$result = $db->prepare($sql);
		$image = array ();
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$row = $result->fetch();
		$image['login'] = $row['login'];
		$image['image'] = $row['image'];
		return $image;
	}

	public static function getAllImagesByUser($id)
	{
		$db = Db::getConnection();
		$sql = "SELECT * FROM photo WHERE user_id = :id ORDER BY id DESC";
		$result =$db->prepare($sql);
		$result->bindParam(':id', $id, PDO::PARAM_INT);
		$result->execute();
		$images = array();
		$i = 0;
		while ($row = $result->fetch()) {
            $images[$i]['login'] = $row['login'];
			$images[$i]['image'] = $row['image'];
			$images[$i]['id'] = $row['id'];
            $i++;
        }
		return $images;
	}

	public static function getAllInfo()
	{
		$db = Db::getConnection();
		$sql = "SELECT count(*) FROM PHOTO";
		$result =$db->prepare($sql);
		$result->execute();
 		$row = $result->fetch();
		return $row['0'];
	}

}

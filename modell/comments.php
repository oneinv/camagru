<?php

class Comments
{
		public static function getAllComments($id)
		{
			$db = Db::getConnection();

			$sql = "SELECT comment,user_id FROM comms WHERE id = :id";

			$result = $db->prepare($sql);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
			$result->execute();
			$coments = array();
			$i = 0;
			while ($row = $result->fetch()) {
				$coments[$i]['comment'] = $row['comment'];
				$coments[$i]['user_id'] = $row['user_id'];
				$i++;
			}
			return $coments;
		}

		public static function addComment($id, $user_id, $comment)
		{
			$db = Db::getConnection();
			$sql = "INSERT INTO comms(id, user_id, comment)
		 			VALUES (:id, :user_id, :comment)";
			$result = $db->prepare($sql);
			$login = Photo::getLoginByPicture($id);
			$isNotific = User::isNotific($login);
			$result->bindParam(':id', $id, PDO::PARAM_INT);
	        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$result->bindParam(':comment', $comment, PDO::PARAM_STR);
			if ($result->execute()){
				if ($isNotific == 1) {
					User::sendMail(User::getMailByLogin($login), "", "notific");
					return true;
				}
			}
			else
				return false;
		}

		public static function deleteComment()
		{
			// TO:DO maybe;
			//not sure that this need;
		}
		
		public static function getAllInfo()
		{
			$db = Db::getConnection();
			$sql = "SELECT count(*) FROM comms";
			$result =$db->prepare($sql);
			$result->execute();
	 		$row = $result->fetch();
			return $row['0'];
		}

}

<?php

class profileController
{

	public function actionIndex()
	{
		$user = User::checkLog();
		require_once(ROOT. '/views/profile/index.php');
		return true;
	}

	public function actionEdit()
	{
		$userName = User::checkLog();
		$userId = User::getIdByLogin($userName);
		$user = User::getUserFullInfo($userName);

		$name = $userName;
		$password = $user['password'];
		$email = $user['email'];
		$notific = $user['notific'];
		$result = false;
		$oldUsr = $name;
		$path = ROOT . '/upload/users/';
		if(isset($_POST['submit'])) {
			$name = htmlspecialchars($_POST['login']);
			$password = htmlspecialchars($_POST['password']);
			$password = hash('sha1', $password);
			$email = htmlspecialchars($_POST['email']);
			$tmp = (!empty($_POST['notific'])) ? true : false;
			if ($tmp == true) {
				$notific = 1;
			} else {
				$notific = 0;
			}
			$errors = false;
			if(!User::checkName($name))
				$errors[] = 'Name cant be less than 2 symbols';
			if(!User::checkPswd($password))
				$errors[] = 'Password cant be less than 6 symbols';
			if(!User::checkMail($email))
				$errors[] = 'Wrong Mail';
			if ($errors == false) {
				$result = User::editData($userId, $name, $password, $email, $notific);
				if ($oldUsr != $name) {
					if(!file_exists($path.$name))
						mkdir($path.$name);
						mkdir($path.$name.$avatar);
				}
			}
		}
		if (isset($_POST['avatar'])) {
			$userName = User::checkLog();
			$target = "upload/users/". $_SESSION['user'].'/avatar/'."avatar".".jpg";
			$image = $_FILES['image']['name'];
			$image = mb_strtolower($image);
			$errors = false;
			$regexp = "~.+(jpg|png|jpeg|gif)$~";
			if (!preg_match($regexp, $image)){
				$errors[] = "wrong file format";
			}
			$name = "avatar.jpg";
			if ($errors == false) {
				Photo::addAvatar($userName, $name);
				if(move_uploaded_file($_FILES['image']['tmp_name'], $target)){
				}
			}
		}
		require_once(ROOT. '/views/profile/edit.php');
		return true;
	}

}

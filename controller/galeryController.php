<?php

class galeryController
{
	public function actionIndex($page = 1)
	{
		$images = array();
		$images = Galery::getAllPictures($page);

		$total = Galery::getNumPictures();
		$pagination = new Pagination($total, $page, Galery::SHOW_BY_DEFAULT, 'page-');

		require_once(ROOT . '/views/galery/index.php');
        return true;
	}

	public function actionView($id = 0)
	{
		$true = 0;
		if ($_SESSION['user'] == Photo::getLoginByPicture($id)) {
			$true = 1;
		}
		$pic = Galery::getImageById($id);
		$coments = Comments::getAllComments($id);
		$likes = Likes::getAllLikes($id);
		if (User::checkLog())
			$userId = User::getIdByLogin($_SESSION['user']);
		$path = '/upload/users'.'/'.$pic['login'].'/' .$pic['image'];
		$errors = false;
		$is_author = false;
		$check = Likes::isLikedByUser($id, $userId);
		if (isset($_POST['submit'])) {
			$comment = htmlspecialchars($_POST['comment']);
			if (strlen($_POST['comment']) == 0)
				$errors[] = "Please enter valid comment";
			if($errors == false) {
				Comments::addComment($id, $userId, $comment);
				header("Refresh: 0");
			}
		}
		if (isset($_POST['like']))
		{
			Likes::addLike($id, $userId);
			header("Refresh: 0");
		}
		if (isset($_POST['unlike']))
		{
			Likes::deleteLike($id, $userId);
			header("Refresh: 0");
		}
		require_once(ROOT . '/views/galery/view.php');

        return true;
	}
}

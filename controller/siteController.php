<?php

class siteController
{

    public function actionIndex()
    {
        require_once(ROOT . '/views/site/index.php');
        return true;
    }

	public function actionStats()
	{

		$info = array();
		$info = '';
		$info['pics'] = galery::getAllInfo();
		$info['users'] = User::getAllInfo();
		$info['likes'] = likes::getAllInfo();
		$info['comments'] = comments::getAllInfo();

 		require_once(ROOT.'/views/site/stats.php');
		return true;
	}

}

<?php

class photoController
{
	public function actionIndex()
	{
		if (!isset($_SESSION['user'])) {
			$errors[] = "You must login for viewing your photos";
			require_once(ROOT. '/views/photo/index.php');
			return true;
		}
		$userId = User::getIdByLogin($_SESSION['user']);
		$userPhotos = array();
		$userPhotos = Galery::getAllImagesByUser($userId);
		require_once(ROOT. '/views/photo/index.php');
		return true;
	}

	public function actionAdd()
	{
		$ok = array("image/png", "image/jpeg", "image/jpg", "image/gif");
		$errors = false;
		if (!isset($_SESSION['user'])) {
			$errors[] = "You must login for adding photos";
			require_once(ROOT. '/views/photo/add.php');
			return true;
		}
		$login = $_SESSION['user'];
		$userId = User::getIdByLogin($login);
		$cur_id = Photo::getLastId();
		if (isset($_POST['submit'])) {
			$target = "upload/users/". $_SESSION['user'].'/'. $cur_id.".jpg";
			$tmp = $cur_id + 1;
			$image = $_FILES['image']['name'];
			$image = mb_strtolower($image);

			$regexp = "~.+(jpg|png|jpeg|gif)$~";
			if (!preg_match($regexp, $image)){
				$errors[] = "wrong file format";
			}
			if ($errors == false) {
				Photo::addPhoto(User::getIdByLogin($_SESSION['user']),$_SESSION['user'], $tmp.".jpg");
				if ($cur_id	== 0) {
					$cur_id = Photo::getLastId();
					Photo::updatePhoto($cur_id);
					$target = "upload/users/". $_SESSION['user'].'/'. $cur_id .".jpg";
				} else if (($cur_id = Photo::getLastId()) != $tmp) {
					Photo::updatePhoto($cur_id);
					$target = "upload/users/". $_SESSION['user'].'/'. $cur_id .".jpg";
				} else {
					$target = "upload/users/". $_SESSION['user'].'/'. $tmp.".jpg";
				}
				if(move_uploaded_file($_FILES['image']['tmp_name'], $target)){
					$type = mime_content_type($target);
					if (in_array($type, $ok)){
						$errors[] = "sucessfuly loaded photo";
					} else {
						Photo::deletePhoto($cur_id, $userId);
						$errors[] = "Wrong file format, trying to play around me?";
					}
				}
				else
					$errors[] = "unsucessfuly loaded photo";
			}
		}
		require_once(ROOT. '/views/photo/add.php');
		return true;
	}

	public function actionWeb()
	{
		if(isset($_SESSION['user'])) {
			$photos = Galery::getAllImagesByUser(User::getIdByLogin($_SESSION['user']));
			$cur_id = Photo::getLastId();
			$errors = false;
			if (isset($_POST['submit'])) {
			  	if ($_POST['value'] == 1) {
					$image = ROOT. "/views/photo/lastweb.png";
					$tmp = $cur_id + 1;
					Photo::addPhoto(User::getIdByLogin($_SESSION['user']),$_SESSION['user'], $tmp.".jpg");
					if ($cur_id	== 0) {
						$cur_id = Photo::getLastId();
						Photo::updatePhoto($cur_id);
						$target = "upload/users/". $_SESSION['user'].'/'. $cur_id .".jpg";
					} else if (($cur_id = Photo::getLastId()) != $tmp) {
						Photo::updatePhoto($cur_id);
						$target = "upload/users/". $_SESSION['user'].'/'. $cur_id .".jpg";
					} else {
						$target = "upload/users/". $_SESSION['user'].'/'. $tmp.".jpg";
					}
					if(rename($image, $target)) {
						header("Location: /view/$cur_id");
					}
				} else
					$errors[] = "make snap first, please";
			}
		}
		require_once(ROOT. '/views/photo/web.php');
		return true;
	}

	public function actionDelete($id = 0)
	{
		$true = 0;
		$errors = false;
		if ($_SESSION['user'] == Photo::getLoginByPicture($id)) {
			$true = 1;
			Photo::deletePhoto($id, User::getIdByLogin($_SESSION['user']));
		}
		require_once(ROOT. '/views/site/index.php');
		return true;
	}
}

<?php


class userController
{
	public function actionRegister()
	{
		$name = '';
		$email = '';
		$password = '';
		$result = false;

		if(isset($_POST['submit'])) {
			$name = htmlspecialchars($_POST['name']);
			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);

			$errors = false;
			if (User::checkLoginExists($name))
				$errors[] = "login already taken";
			if(!User::checkName($name))
				$errors[] = "Wrong name";
			if(!User::checkPswd($password))
				$errors[] = "Wrong password";
			if(!User::checkMail($email))
				$errors[] = "wrong mail";
			if (User::checkMailExists($email))
				$errors[] = "mail already taken";
			$password = hash('sha1', $password);
			if ($errors == false) {
	            $result = User::register($name, $email, $password);
				User::sendMail($email, User::getToken($email), "activate");
	        }
		}
		$name = '';
		$email = '';
		$password = '';
		require_once(ROOT. '/views/user/register.php');

	}

	public function actionLogin()
	{
		$login = '';
		$password = '';
		$msg = '';
		$errors = false;
		if (isset($_POST['submit'])) {
			if (isset($_POST['g-recaptcha-response'])) {
				$url_to_google_api = "https://www.google.com/recaptcha/api/siteverify";

				$secret_key = '6LdFjW8UAAAAAEySyJXDTiWhheKuKyj8tj2OjhFm';
				$query = $url_to_google_api . '?secret=' . $secret_key . '&response='
				. $_POST['g-recaptcha-response'] . '&remoteip=' . $_SERVER['REMOTE_ADDR'];
				$data = json_decode(file_get_contents($query));
				if ($data->success) {
					$login = htmlspecialchars($_POST['login']);
					$password = htmlspecialchars($_POST['password']);
					if(!User::checkName($login))
						$errors[] = 'Wrong mail';
					if (!User::checkPswd($password))
						$errors[] = 'Wrong pswd';
					$password = hash('sha1', $password);
					$userId = User::checkUserData($login, $password);
					if($userId == false) {
						$errors[] = 'Wrong data';
					} else if (!User::checkConfirm($login, $password)) {
						$errors[] = 'Please activate your account on email';
					} else {
						User::auth($userId);
						header("Location: /");
					}
				} else {
					$errors[] = "Please enter Captcha";
				}
			}
		}
		if (isset($_POST['forgot'])) {
			if (strlen(($_POST['login'])) != 0) {
				$login = htmlspecialchars($_POST['login']);
				$info = array();
				$info = User::getUserFullInfo($login);
				if (array_key_exists('email', $info)) {
					User::sendMail($info['email'], $info['token'], "recover");
					$msg = "mail was sucessfuly sent to your email";
				} else {
					$msg = "no such user";
				}
			} else {
 				$errors[] = "Enter login to restore account";
			}
		}
		require_once(ROOT. '/views/user/login.php');
		return true;
	}

	public function actionLogout()
	{
		unset($_SESSION['user']);
		header("Location: /");
	}

	public function actionConfirm($email='', $token='')
	{
		$email = $_GET['email'];
		$TrueToken = User::getToken($email);
		$TrueEmail = User::getMail($email);
		$token = $_GET['token'];
		if($TrueEmail == $email && $TrueToken == $token){
			$result = User::activateUser($email);
		}
		else {
			$result = false;
		}
		require_once(ROOT. '/views/user/confirm.php');
		return true;
	}

	public function actionRecover($email='', $token='')
	{
		$msg = '';
		$email = $_GET['email'];
		$TrueToken = User::getToken($email);
		$TrueEmail = User::getMail($email);
		$token = $_GET['token'];
		if($TrueEmail == $email && $TrueToken == $token){
			$result = true;
		}
		else {
			$result = false;
		}
		if(isset($_POST['submit'])) {
			$email = htmlspecialchars($_POST['email']);
			$password = htmlspecialchars($_POST['password']);

			$errors = false;
			if (!User::checkPswd($password))
				$errors[] = "Pswd not lower than 6 symbols";
			if (!User::checkMail($email))
				$errors[] = "bad format for email";
			if (!User::checkMailExists($email))
				$errors[] = "No such mail";
			if ($_GET['email'] != htmlspecialchars($_POST['email']))
				$errors[] = "You can only edit your profile, brah";
			if ($errors == false) {
				$password = hash('sha1', $password);
	            $result = User::recoverPswd($email, $password);
				$msg = "Password sucessfully changed";
			}
		}
		require_once(ROOT. '/views/user/recover.php');
		return true;
	}
}

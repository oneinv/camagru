<?php include ROOT . '/views/layouts/header.php'; ?>

<?php if (!User::isGuest()) :?>
	<body>
		<p align="center"> Masks </p>
		<img src="/views/photo/masks/cat.png" alt="cat">
		<img src="/views/photo/masks/42.png" alt="catface">
		<img src="/views/photo/masks/doge.png" alt="doge">
		<img src="/views/photo/masks/dogface.png" alt="dogface">
		<img src="/views/photo/masks/kon.png" alt="kon">
		<img src="/views/photo/masks/asuka.png" alt="asuka">
		<img src="/views/photo/masks/dash.png" alt="asuka">

		<p align="center"> Choice your Mask, position</p>
		<div class="container">
	        <div class="row">
				<div class="col-sm-4 col-sm-offset-4 padding-right">
					<select id="select" class="form-control">
						<option value="1">cat</option>
						<option value="2">42 logo</option>
						<option value="3">dog</option>
						<option value="4">dogface</option>
						<option value="5">animegirl</option>
						<option value="6">Asuka</option>
						<option value="7">Dash</option>
					</select>
					<select id="select2" class="form-control">
						<option value="1">left bot</option>
						<option value="2">right bot</option>
						<option value="3">right top</option>
						<option value="4">left top</option>
						<option value="5">in the middle</option>
					</select>
					<?php if (isset($errors) && is_array($errors)): ?>
						<ul>
							<?php foreach ($errors as $error): ?>
								<li> - <?php echo $error; ?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="row">
					<video  id="video" width="640" height="480" autoplay></video>
				   	<button id="snap" class="btn btn-primary btn-sm">Snap Photo</button>
				   	<canvas id="canvas" width="640" height="480"></canvas>
		</div>

		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-4 padding-right">
					<form action="#" method="post">
						<input type="submit" align="center" id="but" name="submit"
						class="btn btn-success" value="Load and view photo">
						<input type="hidden" name="value" value="0" id="lol" />
					</form>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<?php foreach ($photos as $photo): ?>
					<div class="float-right">
						<a href="/view/<?=$photo['id'];?>" class="thumbnail">
							<img  src="<?=galery::getImage($photo['id'], $photo['login']);?>" class="img-rounded pull-xs-left" alt="">
						</a>
					</div>
				<?php endforeach;?>
			</div>
		</div>


	</body>
	<script type="text/javascript">

	var video = document.getElementById('video');

	if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
		navigator.mediaDevices.getUserMedia({
			video: true })
			.then(function(stream) {
				video.srcObject = stream;
			video.play();
		});
	}

	var cnv = document.getElementById('canvas');
	var context = canvas.getContext('2d');
	var video = document.getElementById('video');
	var select = document.getElementById('select');
	var select2 = document.getElementById('select2');
	var btn = document.getElementById('but');
	var l = document.getElementById("lol");
	document.getElementById("snap").addEventListener("click", function() {
		context.drawImage(video, 0, 0, 640, 480);
		l.value = 1;
		var img = cnv.toDataURL('image/png').replace('data:image/png;base64,', '');
		var draw = "";
		var sender = new XMLHttpRequest();
		sender.open("POST", "../views/photo/test1.php", true);
		sender.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		sender.onreadystatechange = function() {
			if (sender.readyState == 4) {
				draw = sender.responseText;
			}
		image = "data:image/png;base64," + draw;
		img = new Image();
		img.onload = function () {
			context.drawImage(img, 0, 0, 640, 480);
		};
		img.src = image;
		};
		var z = select.value;
		var y = select2.value;
		var body = 'Num=' + z +'&img=' + img + '&Corn=' + y;
		sender.send(body);
	})

	</script>

<?php else: ?>
	<body>
		<h1 class="text-center"> LOGIN FIRST, PLEASE </h1>
	</body>
<?php endif;?>

	</html>

<?php include ROOT . '/views/layouts/footer.php'; ?>

<?php include ROOT . '/views/layouts/header.php'; ?>

	<div class="container">
	    <h2 class="title text-center">Your photos</h2>
	    <div class="row">
			<a href="photo/add" class="btn btn-default back"><i class="fa fa-plus"></i> Add photo</a>
			<a href="photo/web" class="btn btn-default back"><i class="fa fa-plus"></i> Add web photo</a> <br />
			<?php if(!empty($userPhotos)):?>
				<?php foreach ($userPhotos as $photo): ?>
					<div class="col-sm-6 col-md-6 col-lg-6">
				    	<a href="/view/<?=$photo['id'];?>" class="thumbnail">
				       		<img  src="<?=galery::getImage($photo['id'], $photo['login']);?>" class="img-fluid" alt="">
				    	</a>
				  	</div>
				<?php endforeach;?>
			<?php else :?>
				<p>No previous photos</p>
			<?php endif;?>
		</div>
	</div>

<?php include ROOT . '/views/layouts/footer.php'; ?>

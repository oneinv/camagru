<?php include ROOT . '/views/layouts/header.php'; ?>
<div class="container">
    <h2 class="title text-center">last photos</h2>
    <div class="row">
		<?php foreach ($images as $image): ?>
		<div class="col-sm-6 col-md-6 col-lg-6">
	    	<a href="/view/<?=$image['id'];?>" class="thumbnail">
	       		<img  src="<?=galery::getImage($image['id'], $image['login']);?>" class="img-fluid" alt="">
	    	</a>
	  	</div>
		<?php endforeach;?>
		<!-- <?=$pagination->get();?> -->
    </div>
</div>
	<div class="container">
		<div class="row">
			<?=$pagination->get();?>
		</div>
	</div>
<?php include ROOT . '/views/layouts/footer.php'; ?>

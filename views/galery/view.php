<?php include ROOT . '/views/layouts/header.php'; ?>

<?php if (isset($errors) && is_array($errors)): ?>
	<ul>
		<?php foreach ($errors as $error): ?>
			<li> - <?= $error; ?></li>
		<?php endforeach; ?>
	</ul>
<?php endif;?>

	<?php if(Photo::getLoginByPicture($id)):?>
		<div class="img">
				<div class="container">
					<div class="row">
					<img src="<?=$path;?>" width ="1100" class ="img-responsive" alt=""/>
					<?php if($true == 1):?>
						<a href="/photo/delete/<?=$id;?>"><button class="btn btn-success">Delete this photo(you must sure)</button></a><br /><br />
					<?php endif;?>
					<p class="fa fa">Share it with your friends in social:</p>
					<div class="social-share">
						  <a href="https://www.facebook.com/sharer.php?u=<?=$_SERVER['HTTP_HOST']."/view/"; ?>&amp;t=<?php echo $pic['image']; ?>" class="social-fb" target="_blank">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						  </a>
						  <a href="https://twitter.com/share?url=<?=$_SERVER['HTTP_HOST']; ?>&amp;text=<?php echo $pic['image']; ?>&amp;hashtags=my_hashtag" class="social-tw" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						  </a>
						  <a href="https://plus.google.com/share?url=<?=$_SERVER['HTTP_HOST']; ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="social-gp">
							<i class="fa fa-google-plus" aria-hidden="true"></i>
						  </a>
						  <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" class="social-pt">
							<i class="fa fa-pinterest" aria-hidden="true"></i>
						  </a>
						  <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $_SERVER['HTTP_HOST']?>&title=<?php echo $pic['image']; ?>&summary=&source=<?php echo $_SERVER['HTTP_HOST']."/view/".$pic['image']; ?>" target="_new">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						  </a>
					</div><br />
					Likes:<?=$likes;?>
					<form action="#" method="post">
						<?php if (Likes::isLikedByUser($id, $userId)) :?>
							<input type="submit" name="unlike" class="btn btn-default" value="unlike">
						<?php else:?>
							<input type="submit" name="like" class="btn btn-default" value="like">
						<?php endif; ?>
					</form>
					<div class="container">
					   <div class="row">
					<?php foreach ($coments as $com) :?>
						<p><?=User::getUserByID($com['user_id'])?> said:</p>
						<p><?=$com['comment'];?></p>
					<?php endforeach;?>
						  	</div>
					</div>
				</div>
				</div>
	</div>
			<section>
			    <div class="container">
			        <div class="row">
							<div class="col-lg-4">
								<div class="login-form">
									<form action="#" method="post">
										<p>Add comment:</p>
										<input type="text" name="comment" placeholder="" value=""
										<br><br>
										<input type="submit" name="submit" class="btn btn-default" value="Comment">
									</form>
								</div>
							</div>
			        </div>
			    </div>
			</section>
<?php else:?>
	<p class = "text-center"> NO THIS PHOTO </p>
<?php endif;?>


<?php include ROOT . '/views/layouts/footer.php'; ?>

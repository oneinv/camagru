<?php include ROOT. '/views/layouts/header.php'; ?>

<section>
	<div class ="container">
		<div class ="row">

			<div class="col-sm-4 col-sm-offset-4 padding-right">
				<?php if ($result) :?>
					<p> Sucessfuly registered, now you should validate your account via email</p>
				<?php else: ?>
					<?php if (isset($errors) && is_array($errors)): ?>
						<ul>
							<?php foreach ($errors as $error): ?>
								<li> - <?=$error; ?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif;?>


				<div class="sign-up-form">
					<h2> Register here please</h2>
					<form action "#" method="post">
						<input type="text" name="name" placeholder="Name" value="<?=$name; ?>"/>
						<input type="email" name="email" placeholder="E-mail" value="<?=$email; ?>"/>
						<input type="password" name="password" placeholder="Password" value="<?=$password;?>"/>
						<input type="submit" name="submit" class="btn btn-default" value="Register" />
					</form>
				</div>
			<?php endif;?>
				<br/>
				<br/>
			</div>
		</div>
	</div>
</section>

<?php include ROOT. '/views/layouts/footer.php'; ?>

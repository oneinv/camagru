<?php include ROOT. '/views/layouts/header.php'; ?>

<section>
	<div class ="container">
		<div class ="row">

			<div class="col-sm-4 col-sm-offset-4 padding-right">
					<?php if (isset($errors) && is_array($errors)): ?>
						<ul>
							<?php foreach ($errors as $error): ?>
								<li> - <?=$error; ?></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>


				<div class="sign-up-form">
					<h2> Login here please</h2>
					<form action "#" method="post">
						<input type="text" name="login" placeholder="" value=""/>
						<input type="password" name="password" placeholder="Password" value=""/>
						<div class="g-recaptcha" data-sitekey="6LdFjW8UAAAAAM_kEXXn4ib22gzXU_B7wRCHDxp4"></div>
						<input type="submit" name="submit" class="btn btn-default" value="Login" />
						<input type="submit" name="forgot" class="btn btn-default" value="Forgot password" />
					</form>
				</div>
				<br/>
				<br/>
				<?=$msg;?>
			</div>
		</div>
	</div>
</section>

<?php include ROOT. '/views/layouts/footer.php'; ?>

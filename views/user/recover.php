<?php include ROOT. '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-4 col-sm-offset-4 padding-right">
				<?php if ($msg): ?>
                    <p>Sucessfull</p>
                <?php else: ?>
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?= $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
					<?php endif;?>
                <?php endif; ?>
				<div class="signup-form">
					<?php if($result): ?>
						<h2>Password Recover</h2>
						<form action="#" method="post">
							<p>Email</p>
							<input type="text" name="email" placeholder="Email" value=""/>
							<p>Password:</p>
							<input type="password" name="password" placeholder="password" value=""/>
							<br/>
							<input type="submit" name="submit" class="btn btn-default" value="Save" />
						</form>
					<?php else :?>
						<h2>Are you cheating me?</h2>
					<?php endif;?>
				</div>
            </div>
        </div>
    </div>
</section>

<?php include ROOT. '/views/layouts/footer.php'; ?>

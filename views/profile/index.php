<?php include ROOT . '/views/layouts/header.php' ;?>

	<div class='container'>
		<div class='row'>
			<h3 class="text-center">
				Hello there,  <?php echo $user ;?>!<br /> Its your profile</h3>
			<img src="<?=User::getAvaByLogin($_SESSION['user']);?>"
				class="img-responsive img-rounded thumbnail center-block"
				style="max-height: 500px; max-width: 500px;">
			<h3 class="text-center">
				<a href="/profile/edit"> Edit you data</a>
			</h3>
		</div>
	</div>
<?php include ROOT. '/views/layouts/footer.php'; ?>

<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-4 col-sm-offset-4 padding-right">

                <?php if ($result): ?>
                    <p>Sucessfuly edited</p>
                <?php else: ?>
                    <?php if (isset($errors) && is_array($errors)): ?>
                        <ul>
                            <?php foreach ($errors as $error): ?>
                                <li> - <?= $error; ?></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>

                    <div class="signup-form">
                        <h2>Profile Editing</h2>
                        <form action="#" method="post">
                            <p>Login</p>
                            <input type="text" name="login" placeholder="Name" value="<?= $name; ?>"/>

							<p>Email</p>
                            <input type="text" name="email" placeholder="Email" value="<?= $email; ?>"/>

                            <p>Password:</p>
                            <input type="password" name="password" placeholder="Пароль" value="<?= $password; ?>"/>
                            <br/>
							 Wanna get email notification?(default = ON)
							<input type="checkbox" name="notific" value="true"></input>
                            <input type="submit" name="submit" class="btn btn-default" value="Save" />
                        </form>
						<form action="#" method="post" enctype="multipart/form-data">
                            <p>Upload Avatar</p>
							<input type="file" name="image" placeholder="" value="">
							<input type="submit" name="avatar" class="btn btn-default" value="Upload" />
                        </form>

                <?php endif; ?>
                <br/>
                <br/>
            </div>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>

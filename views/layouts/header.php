<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Camagru</title>
        <link href="/template/css/bootstrap.min.css" rel="stylesheet">
        <link href="/template/css/font-awesome.min.css" rel="stylesheet">
        <link href="/template/css/prettyPhoto.css" rel="stylesheet">
        <link href="/template/css/price-range.css" rel="stylesheet">
        <link href="/template/css/animate.css" rel="stylesheet">
        <link href="/template/css/main.css" rel="stylesheet">
        <link href="/template/css/responsive.css" rel="stylesheet">
        <link rel="shortcut icon" href="/template/images/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/template/images/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/template/images/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/template/images/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="/template/images/ico/apple-touch-icon-57-precomposed.png">
    </head><!--/head-->

    <body>
        <header id="header"><!--header-->
            <div class="header_top"><!--header_top-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="contactinfo">
                                <ul class="nav nav-pills">
                                    <li><a href=""><i class="fa fa-phone"></i> <?php echo rand(555555,5555555);?></a></li>
                                    <li><a href=""><i class="fa fa-envelope"></i>Nice site you know</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="social-icons pull-right">
                                <ul class="nav navbar-nav">
                                    <li><a href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://plus.google.com/discover"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header_top-->

            <div class="header-middle"><!--header-middle-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="logo pull-left">
                                <a href="/"><img src="/template/images/home/logo.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="shop-menu pull-right">
                                <ul class="nav navbar-nav">
									<?php if (User::isGuest()) :?>
										<li><a href="/user/login/"><i class="fa fa-lock"></i>Login</a></li>
										<li><a href="/user/register/"><i class="fa fa-lock"></i>Register</a></li>
									<?php else: ?>
										<li>
											<a href="/profile/">
												<img src="<?=User::getAvaByLogin($_SESSION['user']);?>"
												class="img-responsive img-rounded"
												style="max-height: 30px; max-width: 30px;">
												(You)
											</a>
										</li>
										<li>
											<a href="/user/logout/">
												<img src="/upload/logout.jpg"
												class="img-responsive img-rounded"
												style="max-height: 30px; max-width: 30px;">
												Log Out
											</a>
										</li>

									<?php endif; ?>
						        </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-middle-->

            <div class="header-bottom"><!--header-bottom-->
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="mainmenu pull-left">
                                <ul class="nav navbar-nav">
                                    <?php if (User::isGuest()) :?>
									<li><a href="/">Main</a></li>
                                    <li><a href="/galery">Galery</a></li>
									<li><a href="/contacts">Info</a></li>
									<?php else: ?>
									<li><a href="/">Main</a></li>
	                                <li><a href="/galery">Galery</a></li>
                                    <li><a href="/photo">Add Photo</a></li>
                                    <li><a href="/contacts">Info</a></li>
									<li><a href="/stats">Stats</a></li>
									<?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/header-bottom-->

        </header><!--/header-->

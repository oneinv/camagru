<?php

return array(

	'user/confirm/?' => 'user/confirm/$1/$2',
	'user/recover/?' => 'user/recover/$1/$2',
	'user/register' => 'user/register',
	'user/login' => 'user/login',
	'user/logout' => 'user/logout',

	'photo/delete' => 'photo/delete',
	'photo/add' => 'photo/add',
	'photo/web' => 'photo/web',
	'photo' => 'photo/index',

	'profile/edit' => 'profile/edit',
    'profile' => 'profile/index',

	'contacts' => 'contacts/index',
	
	'view/([0-9]+)' => 'galery/view/$1',

	'galery/page-([0-9]+)' => 'galery/index/$1',
	'galery' => 'galery/index',

	'stats' => 'site/stats',
    '^/*$' => 'site/index', // actionIndex в SiteController

);
